package com.sok.desetacia1.controllers;

import com.sok.desetacia1.robots.SurinWeb;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class WebController {

    @GetMapping("/web")
    public String webGet(Model model) {
        model.addAttribute("title", "Web страница");
        model.addAttribute("output", "");
        return "web";
    }

    @PostMapping("/web")
    public String webPost(@RequestParam String count, Model model) {
        model.addAttribute("title", "Web страница");
        model.addAttribute("output", count + " : " + SurinWeb.getBits());
        return "web";
    }

}
