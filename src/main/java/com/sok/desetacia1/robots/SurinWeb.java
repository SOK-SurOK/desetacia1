package com.sok.desetacia1.robots;

import java.util.BitSet;

public class SurinWeb {

    public static String getBits() {
        BitSet bits1 = new BitSet(16);
        BitSet bits2 = new BitSet(16);


        // set some bits
        for(int i = 0; i < 16; i++) {
            if((i % 2) == 0) bits1.set(i);
            if((i % 5) != 0) bits2.set(i);
        }

        bits1.and(bits2);
        return bits1.toString();
    }


}
