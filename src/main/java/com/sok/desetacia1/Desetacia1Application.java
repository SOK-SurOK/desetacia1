package com.sok.desetacia1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Desetacia1Application {

    public static void main(String[] args) {
        SpringApplication.run(Desetacia1Application.class, args);
    }

}
